import React, {
  useEffect,
  useState,
} from 'react';
import styled from 'styled-components';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';

import request from '../utils/request';
import JOKE_TYPES from '../utils/jokeTypes';
import { jokesSelectors } from '../store/selectors/jokes';

import JokeSingle from '../common/components/Joke/JokeSingle';
import JokeTwopart from '../common/components/Joke/JokeTwopart';
import Loader from '../common/components/Loader/Loader';
import ErrorMessage from '../common/components/ErrorMessage/ErrorMessage';

const COMMUNICATES = {
  no: 'no',
  yes: 'yes',
};

const DEFAULT_ERROR_MESSAGE = 'Oups, something went wrong :(';

const DetailsPageHeader = styled.header`
  display: grid;
  grid-template-rows: 7rem 1fr;
  width: 100%;
  min-height: 22rem;

  h1 {
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 0;
    font-size: 1.8rem;
    text-align: center;
  }
`;

const DetailsPageAdditionalInfoSection = styled.section`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;

  p {
    flex-grow: 1;
    margin: 0.5rem;
    border-radius: 0.5rem;
    padding: 0.8rem;
    background-color: #efefef;
    font-weight: 300;
    font-size: 1.1rem;
    text-align: center;

    span {
      padding: 0 0.3rem;
      font-weight: 700;
    }
  }
`;

const HomePage = () => {
  const { id } = useParams();

  const initialJoke = useSelector(jokesSelectors.getJokeById(id));

  const [
    joke,
    setJoke,
  ] = useState(initialJoke);

  const [
    error,
    setError,
  ] = useState(null);

  const fetchJoke = async () => {
    try {
      const res = await request.getJoke(id);

      if (res.data.error) throw new Error(DEFAULT_ERROR_MESSAGE);

      await setJoke(res.data);
    } catch (err) {
      setError({
        error: err,
        setError: err.message || DEFAULT_ERROR_MESSAGE,
      });
    }
  };

  useEffect(() => {
    if (!joke) fetchJoke();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  if (error) return <ErrorMessage>{error.message || DEFAULT_ERROR_MESSAGE}</ErrorMessage>;

  if (!joke) return <Loader>Loading...</Loader>;

  return (
    <div>
      <DetailsPageHeader>
        <h1>🤣 Info about joke below 🤣</h1>
        {joke.type === JOKE_TYPES.twopart && (
          <JokeTwopart
            content={{
              delivery: joke.delivery,
              setup: joke.setup,
            }}
          />
        )}
        {joke.type === JOKE_TYPES.single && (
          <JokeSingle content={joke} />
        )}
      </DetailsPageHeader>
      <DetailsPageAdditionalInfoSection>
        <p>
          category -
          <span>{joke.category}</span>
        </p>
        <p>
          type -
          <span>{joke.type}</span>
        </p>
        <p>
          id -
          <span>{joke.id}</span>
        </p>
      </DetailsPageAdditionalInfoSection>
      <DetailsPageAdditionalInfoSection>
        <p>
          is this joke sexist?
          <span>{joke.flags.sexist ? COMMUNICATES.yes : COMMUNICATES.no}</span>
        </p>
        <p>
          is this joke racist?
          <span>{joke.flags.racist ? COMMUNICATES.yes : COMMUNICATES.no}</span>
        </p>
        <p>
          is this joke political?
          <span>{joke.flags.political ? COMMUNICATES.yes : COMMUNICATES.no}</span>
        </p>
        <p>
          is this joke religiuos?
          <span>{joke.flags.religious ? COMMUNICATES.yes : COMMUNICATES.no}</span>
        </p>
      </DetailsPageAdditionalInfoSection>
    </div>
  );
};

export default HomePage;
