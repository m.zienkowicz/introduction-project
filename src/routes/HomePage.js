import React from 'react';
import styled from 'styled-components';

import RandomJokeSection from '../common/containers/RandomJokeSection/RandomJokeSection';
import JokesList from '../common/containers/JokesList/JokesList';

const HomePageHeader = styled.section`
  display: grid;
  grid-template-rows: 7rem 5rem 1fr;
  width: 100%;
  min-height: 28rem;

  h1 {
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 0;
    font-size: 1.8rem;
    text-align: center;
  }
`;

const HomePage = () => (
  <div>
    <HomePageHeader>
      <h1>🤣 Smile app - give yourself some hapiness 🤣</h1>
      <RandomJokeSection />
    </HomePageHeader>
    <JokesList />
  </div>
);

export default HomePage;
