import React from 'react';
import {
  Route,
  BrowserRouter as Router,
  Switch,
} from 'react-router-dom';
import { createGlobalStyle } from 'styled-components';
import {
  Helmet,
  HelmetProvider,
} from 'react-helmet-async';
import { Provider as StoreProvider } from 'react-redux';

import configureStore from './store/store';

import HomePage from './routes/HomePage';
import DetailsPage from './routes/DetailsPage';

const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
  }

  body {
    margin: 0;
    padding: 1rem;
    font-family: 'Lato', sans-serif;
  }

  .app-wrapper {
    max-width: 1024px;
    margin: 0 auto;
  }
`;

function App() {
  const store = configureStore();

  return (
    <StoreProvider store={store}>
      <HelmetProvider>
        <Router>
          <div className="app-wrapper">
            <Helmet>
              <link
                rel="preconnect"
                href="https://fonts.gstatic.com"
              />
              <link
                href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&display=swap"
                rel="stylesheet"
              />
            </Helmet>
            <Switch>
              <Route
                path="/"
                exact
                component={HomePage}
              />
              <Route
                path="/details/:id"
                exact
                component={DetailsPage}
              />
            </Switch>
            <GlobalStyle />
          </div>
        </Router>
      </HelmetProvider>
    </StoreProvider>
  );
}

export default App;
