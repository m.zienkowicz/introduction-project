export const jokesSelectors = {
  getError: state => state.jokes.error,
  getIsFetching: state => state.jokes.isFetching,
  getJokeById: id => state => state.jokes.list.find(item => item.id.toString() === id),
  getJokes: state => state.jokes.list,
};
