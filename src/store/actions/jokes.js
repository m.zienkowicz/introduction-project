import request from '../../utils/request';

export const GET_JOKES_REQUEST = 'GET_JOKES_REQUEST';
export const GET_JOKES_SUCCESS = 'GET_JOKES_SUCCESS';
export const GET_JOKES_FAILURE = 'GET_JOKES_FAILURE';

const getJokesRequest = () => ({
  type: GET_JOKES_REQUEST,
});

const getJokesSuccess = payload => ({
  payload,
  type: GET_JOKES_SUCCESS,
});

const getJokesFailure = payload => ({
  payload,
  type: GET_JOKES_FAILURE,
});

export const getJokes = (resultToLoad, isScope) => dispatch => {
  dispatch(getJokesRequest());

  return request
    .getJokes(resultToLoad, isScope)
    .then(({ data }) => {
      dispatch(getJokesSuccess(data));

      return data;
    })
    .catch(error => {
      dispatch(getJokesFailure(error));

      return error;
    });
};

