import { combineReducers } from 'redux';

import jokes from './jokes';

const reducers = combineReducers({
  jokes,
});

export default reducers;
