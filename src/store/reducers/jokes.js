import {
  GET_JOKES_FAILURE,
  GET_JOKES_REQUEST,
  GET_JOKES_SUCCESS,
} from '../actions/jokes';

const defaultErrorMessage = 'Oups, something went wrong';

const initialState = {
  error: null,
  isFetching: false,
  list: [],
};

const jokesReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_JOKES_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case GET_JOKES_SUCCESS:
      return {
        ...state,
        error: null,
        isFetching: false,
        list: [
          ...state.list,
          ...action.payload.jokes,
        ],
      };
    case GET_JOKES_FAILURE:
      return {
        ...state,
        error: {
          error: action.payload,
          message: action.payload?.message || defaultErrorMessage,
        },
        isFetching: false,
      };
    default:
      return state;
  }
};

export default jokesReducer;
