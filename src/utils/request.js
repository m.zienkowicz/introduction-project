import axios from 'axios';

const BASE_URL = 'https://v2.jokeapi.dev/joke/Any';

class Request {
  getJoke(idRange) {
    if (!idRange) return axios.get(BASE_URL);

    return axios.get(`${BASE_URL}?idRange=${idRange}`);
  }

  getJokes(amount = 10, idRange) {
    if (!idRange) return axios.get(`${BASE_URL}?amount=${amount}`);

    return axios.get(`${BASE_URL}?amount=${amount}&idRange=${idRange}`);
  }

  get(...args) {
    return axios.get(...args);
  }

  post(...args) {
    return axios.post(...args);
  }

  options(...args) {
    return axios.options(...args);
  }

  patch(...args) {
    return axios.patch(...args);
  }
}

export default new Request();
