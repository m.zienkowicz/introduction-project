import styled from 'styled-components';

const ErrorMessage = styled.p`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 1rem 0;
  color: red;
`;

export default ErrorMessage;
