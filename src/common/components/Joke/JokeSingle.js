import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const JokeWrapper = styled.article`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 1rem 0;
  border: 0.2rem solid royalblue;
  border-radius: 1rem;
  padding: 1rem 3rem;
  color: black;
  font-weight: 300;
  font-size: 1.3rem;
  line-height: 150%;
  text-align: center;
`;

const JokeSingle = ({ content }) => (
  <JokeWrapper>
    <p>{content.joke}</p>
  </JokeWrapper>
);

export default JokeSingle;

JokeSingle.propTypes = {
  content: PropTypes.shape({
    joke: PropTypes.string,
  }),
};

JokeSingle.defaultProps = {
  content: {
    joke: '',
  },
};
