import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const JokeWrapper = styled.article`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 1rem 0;
  border: 0.2rem solid royalblue;
  border-radius: 1rem;
  padding: 1rem 3rem;
  color: black;
  font-weight: 300;
  line-height: 150%;
  text-align: center;

  p {
    margin: 0.5rem;
    font-size: 1.3rem;
  }

  p:nth-child(1) {
    font-weight: bold;
  }
`;

const JokeTwopart = ({ content }) => (
  <JokeWrapper>
    <p>{content.setup}</p>
    <p>{content.delivery}</p>
  </JokeWrapper>
);

export default JokeTwopart;

JokeTwopart.propTypes = {
  content: PropTypes.shape({
    delivery: PropTypes.string,
    setup: PropTypes.string,
  }),
};

JokeTwopart.defaultProps = {
  content: {
    delivery: '',
    setup: '',
  },
};
