import styled from 'styled-components';

const Loader = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 1rem 0;
`;

export default Loader;
