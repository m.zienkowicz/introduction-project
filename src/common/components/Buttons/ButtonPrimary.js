import styled from 'styled-components';

const ButtonPrimary = styled.button`
  display: block;
  width: auto;
  border: 0.1rem solid royalblue;
  border-radius: 1rem;
  padding: 0.75rem 2rem;
  background-color: royalblue;
  color: white;
  font-weight: bold;
  font-size: 1.2rem;
  cursor: pointer;

  &[disabled] {
    border: 0.1rem solid #999;
    background-color: #aaa;
  }
`;

export default ButtonPrimary;
