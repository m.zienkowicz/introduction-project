import React, { useEffect } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import {
  useDispatch,
  useSelector,
} from 'react-redux';

import { getJokes } from '../../../store/actions/jokes';
import { jokesSelectors } from '../../../store/selectors/jokes';

import JOKE_TYPES from '../../../utils/jokeTypes';

import PrimaryButton from '../../components/Buttons/ButtonPrimary';
import ErrorMessage from '../../components/ErrorMessage/ErrorMessage';
import Loader from '../../components/Loader/Loader';
import JokeTwopart from '../../components/Joke/JokeTwopart';
import JokeSingle from '../../components/Joke/JokeSingle';

const JokesListWrapper = styled.section`
  width: 100%;
  max-width: 1024px;
  margin: 0 auto;

  h3 {
    font-size: 2rem;
    text-align: center;
  }

  button {
    margin: 0 auto;
  }

  ul {
    margin: 0;
    padding: 0;
    list-style: none;
  }

  a {
    text-decoration: none;
    cursor: pointer;

    article:hover {
      background-color: #efefef;
    }
  }
`;

const resultToLoad = 10;

const JokesList = () => {
  const jokes = useSelector(jokesSelectors.getJokes);
  const error = useSelector(jokesSelectors.getError);
  const isFetching = useSelector(jokesSelectors.getIsFetching);

  const idScope = `${jokes.length + 1}-${jokes.length + resultToLoad}`;

  const dispatch = useDispatch();

  const getMultipleJokes = () => dispatch(getJokes(resultToLoad, idScope));

  useEffect(() => {
    if (!jokes.length) getMultipleJokes();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <JokesListWrapper>
      <h3>Jokes list</h3>
      <ul>
        {
          jokes.map(({
            type,
            id,
            joke,
            delivery,
            setup,
          }) => (
            <li key={id}>
              <Link to={`details/${id}`}>
                {type === JOKE_TYPES.single && (
                  <JokeSingle content={{ joke }} />
                )}
                {type === JOKE_TYPES.twopart && (
                  <JokeTwopart
                    content={{
                      delivery,
                      setup,
                    }}
                  />
                )}
              </Link>
            </li>
          ))
        }
      </ul>
      {isFetching && <Loader>Loading...</Loader>}
      {error && <ErrorMessage>{error.message}</ErrorMessage>}
      <PrimaryButton
        onClick={getMultipleJokes}
        type="button"
        disabled={isFetching}
      >
        Load more!
      </PrimaryButton>
    </JokesListWrapper>
  );
};

export default JokesList;
