import React, { useState } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import request from '../../../utils/request';
import JOKE_TYPES from '../../../utils/jokeTypes';

import JokeSingle from '../../components/Joke/JokeSingle';
import JokeTwopart from '../../components/Joke/JokeTwopart';
import Loader from '../../components/Loader/Loader';
import ErrorMessage from '../../components/ErrorMessage/ErrorMessage';
import ButtonPrimary from '../../components/Buttons/ButtonPrimary';

const RandomJokeSectionWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  button {
    margin: 0 auto;
  }

  a {
    text-decoration: none;
    cursor: pointer;

    article:hover {
      background-color: #efefef;
    }
  }
`;

const RandomJokeSection = () => {
  const [
    joke,
    setJoke,
  ] = useState(false);
  const [
    isLoading,
    setLoading,
  ] = useState(false);
  const [
    error,
    setError,
  ] = useState(null);

  const getJoke = async () => {
    await setError(null);
    await setLoading(true);

    try {
      const res = await request.getJoke();

      await setJoke(res.data);
      await setLoading(false);
    } catch (err) {
      await setError({
        error: err,
        message: 'Oups, something went wrong, check your internet connection!',
      });
      await setLoading(false);
    }
  };

  const shouldJokeRender = !error && joke && !isLoading;

  return (
    <>
      <RandomJokeSectionWrapper>
        <ButtonPrimary
          onClick={getJoke}
          type="button"
          disabled={isLoading}
        >
          Get a random joke!
        </ButtonPrimary>
      </RandomJokeSectionWrapper>
      <RandomJokeSectionWrapper>
        {error && (
          <ErrorMessage>{error.message}</ErrorMessage>
        )}
        {isLoading && (
          <Loader>Loading...</Loader>
        )}
        {(shouldJokeRender && joke.type === JOKE_TYPES.single) && (
          <Link to={`details/${joke.id}`}>
            <JokeSingle content={joke} />
          </Link>
        )}
        {(shouldJokeRender && joke.type === JOKE_TYPES.twopart) && (
          <Link to={`details/${joke.id}`}>
            <JokeTwopart content={joke} />
          </Link>
        )}
      </RandomJokeSectionWrapper>
    </>
  );
};

export default RandomJokeSection;
